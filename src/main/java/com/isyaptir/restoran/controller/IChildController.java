package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

interface IChildController<T> extends IController<T> {

    @PostMapping(value = "/create/{parentId}")
    Response save(@RequestBody T t, @PathVariable String parentId);

}
