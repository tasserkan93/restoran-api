package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

interface IParentController<T> extends IController<T> {

    @PostMapping(value = "/create")
    Response save(T t);
}
