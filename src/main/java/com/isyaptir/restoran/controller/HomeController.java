package com.isyaptir.restoran.controller;

import org.mapstruct.Context;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/")
@ApiIgnore
class HomeController {

    @GetMapping
    public String remoteIp(@Context HttpServletRequest request) {
        //return request.getLocalAddr();
        return "Api dokümanına <a href='/swagger-ui.html'> buradan</a> ulaşabilirsiniz";
    }
}
