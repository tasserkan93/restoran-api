package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Menu;
import com.isyaptir.restoran.model.Product;
import com.isyaptir.restoran.model.Unit;
import com.isyaptir.restoran.service.MenuService;
import com.isyaptir.restoran.service.ProductService;
import com.isyaptir.restoran.service.UnitService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "product")
class ProductController implements IChildController<Product> {

    @Autowired
    private ProductService productService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private UnitService unitService;

    @Override
    public Response findById(String id) {

        Product product = productService.findById(id);
        return Response.ok(product);
    }

    @Override
    public Response findAll() {
        List<Product> products = productService.findAll();
        return Response.ok(products);
    }

    /*TODO : Ürün kaydederken birim sadece birim idsini veya birim nesnesini göndererek kaydedebilir miyiz?*/
    @ApiOperation(value = "Parent: Menu")
    @Override
    public Response save(Product product, String parentId) {
        Menu menu = menuService.findById(parentId);
        try {
            product.setMenu(menu);
            Unit unit = unitService.findById(product.getUnit().getID());
            product.setUnit(unit);
            return Response.created(productService.save(product));

        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response delete(String id) {
        try {
            UUID gcRecord = UUID.randomUUID();
            Product product = productService.findById(id);
            product.setGcRecord(gcRecord);
            return Response.ok(productService.save(product));
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response update(Product Product, String id) {
        try {
            Product product = productService.findById(id);
            product.setProductName(Product.getProductName());
            product.setCoverImage(Product.getCoverImage());
            product.setKdvRate(Product.getKdvRate());
            product.setPrice(Product.getPrice());
            product.setUnitQuantities(Product.getUnitQuantities());
            Unit unit = unitService.findById(Product.getUnit().getID());
            product.setUnit(unit);
            product = productService.save(product);
            return Response.ok(product);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }
}
