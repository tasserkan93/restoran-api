package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Area;
import com.isyaptir.restoran.service.AreaService;
import com.isyaptir.restoran.service.FileSystemStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "area")
public class AreaController implements IParentController<Area> {

    @Autowired
    private AreaService service;

    @Autowired
    private FileSystemStorageService fileSystemStorageService;

    public Response findById(String id) {
        Area area = service.findById(id);
        return Response.ok(area);
    }

    public Response findAll() {
        List<Area> areas = service.findAll();
        return Response.ok(areas);
    }

    @Override
    public Response save(@Valid @RequestBody Area area) {
        try {
            return Response.created(service.save(area));
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response delete(String id) {

        try {
            UUID gcRecord = UUID.randomUUID();
            Area area = service.findById(id);
            area.setGcRecord(gcRecord);
            area = service.save(area);
            return Response.ok(area);

        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }

    }

    @Override
    public Response update(Area reqArea, String id) {
        try {
            Area area = service.findById(id);
            area.setAreaName(reqArea.getAreaName());
            area.setCreateUser(reqArea.getCreateUser());
            area = service.save(area);
            return Response.ok(area);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }
}

