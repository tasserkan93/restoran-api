package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Customer;
import com.isyaptir.restoran.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "customer")
class CustomerController implements IParentController<Customer> {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response findById(String id) {
        Customer customer = customerService.findById(id);
        return Response.ok(customer);
    }

    @Override
    public Response findAll() {
        List<Customer> customers = customerService.findAll();
        return Response.ok(customers);
    }

    @Override
    public Response save(@RequestBody Customer customer) {

        try {
            Customer customerResult = customerService.save(customer);
            return Response.created(customerResult);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }


    @Override
    public Response delete(String id) {
        try {
            UUID gcRecord = UUID.randomUUID();
            Customer customer = customerService.findById(id);
            customer.setGcRecord(gcRecord);
            customer = customerService.save(customer);
            return Response.ok(customer);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response update(Customer customer, String id) {
        try {
            Customer customerResult = customerService.findById(id);
            customerResult.setAddress(customer.getAddress());
            customerResult.setName(customer.getName());
            customerResult.setPhone(customer.getPhone());
            customerResult.setSurname(customer.getSurname());
            return Response.ok(customerService.save(customerResult));
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }
}
