package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

interface IController<T> {

    @GetMapping(value = "/{id}")
    Response findById(@PathVariable String id);

    @GetMapping("/list")
    Response findAll();

    @DeleteMapping("/delete/{id}")
    Response delete(@PathVariable String id);

    @PutMapping(value = "/update/{id}")
    Response update(@RequestBody T t, @PathVariable String id);
}
