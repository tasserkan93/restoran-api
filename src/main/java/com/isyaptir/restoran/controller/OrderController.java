package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Order;
import com.isyaptir.restoran.model.Product;
import com.isyaptir.restoran.model.Table;
import com.isyaptir.restoran.service.OrderService;
import com.isyaptir.restoran.service.ProductService;
import com.isyaptir.restoran.service.RTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Optional;

@RestController
@RequestMapping(Constants.API_URL + "order")
class OrderController implements IController<Order> {

    @Autowired
    private OrderService service;
    @Autowired
    private RTableService tableService;
    @Autowired
    private ProductService productService;

    @Override
    public Response findById(String id) {
        Order order = service.findById(id);
        return Response.ok(order);
    }

    @Override
    public Response findAll() {
        List<Order> orders = service.findAll();
        return Response.ok(orders);
    }

    @PostMapping("/create")
    public Response save(@RequestBody Order order) {
        Table table = tableService.findById(order.getTable().getID());
        if (!table.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Dolu masaya yeniden sipariş girilemez");
        }

        List<Product> products = new ArrayList<>();
        order.getPProducts()
                .stream()
                .map(product -> productService.findById(product.getID()))
                .forEach(products::add);
        order.setTable(table);
        order.setPProducts(products);
        return Response.created(order);
    }

    @Override
    public Response delete(String id) {
        Order order = service.findById(id);
        UUID gcRecord = UUID.randomUUID();
        order.setGcRecord(gcRecord);
        return Response.ok(service.save(order));
    }

    @Override
    public Response update(Order Order, String id) {
        Order order = service.findById(id);
        Table table = tableService.findById(Order.getTable().getID());
        order.setTable(table);
        List<Product> products = Order.getPProducts();
        order.setPProducts(products);
        return Response.ok(service.save(order));
    }

    public Response addProduct(@RequestBody Product product, @PathVariable String orderId) {
        Optional<Order> optionalOrder = Optional.of(service.findById(orderId));
        return null;
    }

    public Response addProducts(@RequestBody List<Product> products, @PathVariable String orderId) {
        return null;
    }

    @GetMapping("product/remove/{orderId}/{productId}")
    public Response removeProduct(@PathVariable String orderId, @PathVariable String productId) {
        Order order = service.findById(orderId);
        Product product = productService.findById(productId);
        order.getPProducts().remove(product);
        return Response.ok(service.save(order));
    }

    @GetMapping("table/move/{orderId}/{tableId}")
    public Response moveTable(@PathVariable String orderId, @PathVariable String tableId) {
        Order order = service.findById(orderId);
        Table table = tableService.findById(tableId);
        order.getTable().setEmpty(false);
        table.setEmpty(true);
        order.setTable(table);
        return Response.ok(service.save(order));
    }

}