package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Body;
import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Menu;
import com.isyaptir.restoran.service.FileSystemStorageService;
import com.isyaptir.restoran.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "menu")
class MenuController implements IParentController<Menu> {

    @Autowired
    private MenuService menuService;

    @Autowired
    private FileSystemStorageService storage;

    @Override
    public Response findById(String id) {
        Menu menu = menuService.findById(id);
        return Response.ok(menu);
    }

    @GetMapping("/find/{id}")
    public Response findBy(@PathVariable String id) {
        Menu menu = menuService.findById(id);
        return Response.ok(menu);
    }

    @Override
    public Response findAll() {
        List<Menu> menus = menuService.findAll();
        return Response.ok(menus);
    }

    @Override
    public Response delete(String id) {
        try {
            UUID gcRecord = UUID.randomUUID();
            Menu menu = menuService.findById(id);
            menu.setGcRecord(gcRecord);
            menu = menuService.save(menu);
            return Response.ok(menu);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response update(Menu menu, String id) {
        try {
            Menu m = menuService.findById(id);
            m.setCoverImage(menu.getCoverImage());
            m.setMenuName(menu.getMenuName());
            m = menuService.save(m);
            return Response.ok(m);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response save(@ModelAttribute Menu menu) {
        try {
            MultipartFile multipartFile = menu.getCoverImage();
            String file = storage.store(multipartFile, "/menu");
            menu.setImage(file);
            menu = menuService.save(menu);
            return Response.created(menu);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }
}
