package com.isyaptir.restoran.controller.auth;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.model.auth.User;
import com.isyaptir.restoran.service.auth.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/settings")

public class UserController {

    @Autowired
    private UserDetailService userDetailService;

    @RequestMapping("create/user")
    @PostMapping
    public Response save(@RequestBody User user) {

        user.setPassword("{bcrypt}" + BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)));

        return Response.ok(userDetailService.save(user));
    }

    @GetMapping("users")
    public Response findAll() {
        List<User> users = userDetailService.findAll();
        return Response.ok(users);

    }
}
