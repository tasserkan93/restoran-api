package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;

import com.isyaptir.restoran.model.Unit;
import com.isyaptir.restoran.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "unit")
class UnitController implements IParentController<Unit> {

    @Autowired
    private UnitService service;

    @Override
    public Response findById(String id) {
        Unit unit = service.findById(id);
        return Response.ok(unit);
    }

    @Override
    public Response findAll() {
        List<Unit> units = service.findAll();
        return Response.ok(units);
    }

    @Override
    public Response save(@RequestBody Unit unit) {
        try {
            unit = service.save(unit);
            return Response.created(unit);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response delete(String id) {
        try {
            Unit unit = service.findById(id);
            UUID gcRecord = UUID.randomUUID();
            unit.setGcRecord(gcRecord);
            unit = service.save(unit);
            return Response.ok(unit);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response update(Unit unit, String id) {
        return null;
    }

    @GetMapping("/code/{code}")
    public Response findByCode(@PathVariable String code) {
        Unit unit = service.findByCode(code);
        return Response.ok(unit);
    }
}
