package com.isyaptir.restoran.controller;

import com.isyaptir.restoran.helper.Response;
import com.isyaptir.restoran.security.Constants;
import com.isyaptir.restoran.model.Area;
import com.isyaptir.restoran.model.Table;
import com.isyaptir.restoran.service.AreaService;
import com.isyaptir.restoran.service.RTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(Constants.API_URL + "table")
class RTableController implements IChildController<Table> {

    @Autowired
    private RTableService tableService;
    @Autowired
    private AreaService areaService;

    @Override
    public Response save(Table table, String parentId) {

        try {
            Area area = areaService.findById(parentId);
            table.setArea(area);
            return Response.created(tableService.save(table));
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response findById(String id) {
        Table table = tableService.findById(id);
        return Response.ok(table);
    }

    @Override
    public Response findAll() {
        return Response.ok(tableService.findAll());
    }

    @Override
    public Response delete(String id) {
        try {
            Table table = tableService.findById(id);
            UUID gcRecord = UUID.randomUUID();
            table.setGcRecord(gcRecord);
            table = tableService.save(table);
            return Response.ok(table);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }

    @Override
    public Response update(Table paramTable, String id) {
        try {
            Table table = tableService.findById(id);
            table.setTableName(paramTable.getTableName());
            paramTable = tableService.save(table);
            return Response.ok(paramTable);
        } catch (Exception e) {
            return Response.internalErr(e.getMessage());
        }
    }
}