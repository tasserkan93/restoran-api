package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, String> {
}
