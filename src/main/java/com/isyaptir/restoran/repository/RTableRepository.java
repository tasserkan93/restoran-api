package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RTableRepository extends CrudRepository<Table, String> {

}