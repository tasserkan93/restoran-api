package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Unit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitRepository extends CrudRepository<Unit, String> {

    Unit findByCode(String code);
}
