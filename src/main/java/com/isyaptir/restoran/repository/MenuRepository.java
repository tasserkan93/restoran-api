package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Menu;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends CrudRepository<Menu, String> {

    @Query("SELECT m FROM Menu m where gcRecord=null")
    List<Menu> findAll();
}
