package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Area;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface AreaRepository extends CrudRepository<Area, String> {

    @Query("SELECT a FROM Area a WHERE gcRecord=null")
    List<Area> findAll();

    @Query("SELECT a FROM Area a WHERE gcRecord=null and id=?1")
    Optional<Area> findById(String id);
}
