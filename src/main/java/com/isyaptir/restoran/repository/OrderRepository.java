package com.isyaptir.restoran.repository;

import com.isyaptir.restoran.model.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface OrderRepository extends CrudRepository<Order, String> {


    @Query("SELECT p FROM Order p WHERE gcRecord=null and id=?1")
    Optional<Order> findById(String id);
}
