package com.isyaptir.restoran.helper;

import org.springframework.http.HttpStatus;

public class Body {

    private HttpStatus status;
    private int code;
    private Object body;

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
