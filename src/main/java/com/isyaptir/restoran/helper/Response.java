package com.isyaptir.restoran.helper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

public class Response extends HashMap {

    private static Response map = new Response();
    private final static String STATUS = "status";
    private final static String DATA = "data";
    private final static String COUNT = "count";
    private final static String MSG = "message";

    public static <T> Response ok(T data) {
        map.clear();
        map.put(STATUS, HttpStatus.OK.value());
        if (isArray(data)) {
            ArrayList list = (ArrayList) data;
            map.put(COUNT, list.size());
        }
        map.put(DATA, data);
        return map;
    }

    public static <T> Response created(T data) {
        map.clear();
        map.put(STATUS, HttpStatus.CREATED.value());
        map.put(DATA, data);
        return map;
    }

    public static Response internalErr(String message) {
        map.clear();
        map.put(STATUS, HttpStatus.INTERNAL_SERVER_ERROR.value());
        map.put(MSG, message);
        return map;
    }

    private static boolean isArray(Object obj) {
        return obj instanceof List<?>;
    }

}
