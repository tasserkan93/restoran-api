package com.isyaptir.restoran.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@javax.persistence.Table(name = "Area")
public class Area extends BaseModel {

    @Column
    @NotNull(message = "Alan boş olamaz!")
    private String areaName;
    @Column
    private long createUser;

    @OneToMany(mappedBy = "area")
    private Set<Table> tables;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(long createUser) {
        this.createUser = createUser;
    }


    public Set<Table> getTables() {
        return tables;
    }

    public void setTables(Set<Table> tables) {
        this.tables = tables;
    }
}
