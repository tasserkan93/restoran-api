package com.isyaptir.restoran.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "PProduct")
public class Product extends BaseModel {

    @Column
    private String productName;

    @Transient
    @JsonIgnore
    private MultipartFile coverImage;

    @Column
    private double price;

    @Column
    private double unitQuantities;

    @ManyToOne(cascade = CascadeType.ALL)
    private Unit unit;

    @ManyToOne
    @JsonIgnore
    private Menu menu;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

    @Column
    private long kdvRate;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public MultipartFile getCoverImage() {
        return coverImage;
    }

    @JsonIgnore
    public void setCoverImage(MultipartFile coverImage) {
        this.coverImage = coverImage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Menu getMenu() {
        return this.menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public long getKdvRate() {
        return kdvRate;
    }

    public void setKdvRate(long kdvRate) {
        this.kdvRate = kdvRate;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @JsonIgnore
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrder(List<Order> orders) {
        this.orders = orders;
    }

    public double getUnitQuantities() {
        return unitQuantities;
    }

    public void setUnitQuantities(double unitQuantities) {
        this.unitQuantities = unitQuantities;
    }
}
