package com.isyaptir.restoran.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@javax.persistence.Table(name = "POrder")
public class Order extends BaseModel {

    @NotNull(message = "Masa boş olamaz")
    @ManyToOne
    @JoinColumn(name = "table_id")
    private com.isyaptir.restoran.model.Table table;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "order_product",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id")
    )
    private List<Product> products;

    public com.isyaptir.restoran.model.Table getTable() {
        return table;
    }

    public void setTable(com.isyaptir.restoran.model.Table table) {
        this.table = table;
    }


    public List<Product> getPProducts() {
        return products;
    }

    public void setPProducts(List<Product> products) {
        this.products = products;
    }
}
