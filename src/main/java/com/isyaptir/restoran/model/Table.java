package com.isyaptir.restoran.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "PTable")
public class Table extends BaseModel {

    @Column
    private String tableName;

    @Column
    private boolean isEmpty;

    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area area;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "table")
    private List<Order> Orders;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @JsonIgnore
    public Area getArea() {
        return area;
    }

    @JsonIgnore
    public List<Order> getOrder() {
        return Orders;
    }

    public void setOrder(List<Order> Order) {
        this.Orders = Order;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
}