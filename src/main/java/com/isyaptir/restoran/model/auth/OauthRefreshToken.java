package com.isyaptir.restoran.model.auth;


import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table(name = "oauth_refresh_token")
class OauthRefreshToken {

    @Id
    private String tokenId;
    @Column
    private Blob token;
    @Column
    private Blob authentication;

    public Blob getToken() {
        return token;
    }

    public void setToken(Blob token) {
        this.token = token;
    }

    public Blob getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Blob authentication) {
        this.authentication = authentication;
    }

    public String getRefreshToken() {
        return tokenId;
    }

    public void setRefreshToken(String tokenId) {
        this.tokenId = tokenId;
    }
}
