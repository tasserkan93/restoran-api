package com.isyaptir.restoran.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "Unit")
public class Unit extends BaseModel {

    @Column
    private String title;
    @Column
    private String code;

    @OneToMany(mappedBy = "unit")
    private List<Product> products;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonIgnore
    public List<Product> getPProduct() {
        return products;
    }

    public void setPProduct(List<Product> Product) {
        this.products = Product;
    }
}
