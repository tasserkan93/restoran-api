package com.isyaptir.restoran.security;

public class Constants {

    /*
     * Message Constants
     * */
    public static final String MSG_NOT_FOUND = "İlgili kayıt bulunamadı. RecordID:%s";

    public static final String API_URL = "/api/v1/";

}
