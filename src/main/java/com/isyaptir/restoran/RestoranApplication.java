package com.isyaptir.restoran;

import com.isyaptir.restoran.security.StoragePropertiesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StoragePropertiesConfig.class)
class RestoranApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestoranApplication.class, args);
    }

}
