package com.isyaptir.restoran.service;

import java.util.List;

interface IService<T> {

    T findById(String id);

    List<T> findAll();

    T delete(T t);

    T save(T t);

    T update(T t);
}
