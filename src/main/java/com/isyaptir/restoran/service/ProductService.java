package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Product;
import com.isyaptir.restoran.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements IService<Product> {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findById(String id) {
        Optional<Product> product = productRepository.findById(id);
        product.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li ürün bulunamadı"));
        return product.get();
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product delete(Product Product) {
        return productRepository.save(Product);
    }

    @Override
    public Product save(Product Product) {
        return productRepository.save(Product);
    }

    @Override
    public Product update(Product Product) {
        return productRepository.save(Product);
    }
}
