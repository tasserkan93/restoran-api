package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Table;
import com.isyaptir.restoran.repository.RTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class RTableService implements IService<Table> {

    @Autowired
    private RTableRepository repository;

    @Override
    public Table findById(String id) {
        Optional<Table> rTable = repository.findById(id);
        rTable.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li masa bulunamadı"));
        return rTable.get();
    }

    @Override
    public List<Table> findAll() {
        return (List<Table>) repository.findAll();
    }

    @Override
    public Table delete(Table table) {
        return repository.save(table);
    }

    @Override
    public Table save(Table table) {
        return repository.save(table);
    }

    @Override
    public Table update(Table table) {
        return repository.save(table);
    }


}