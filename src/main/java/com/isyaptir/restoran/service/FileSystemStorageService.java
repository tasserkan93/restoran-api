package com.isyaptir.restoran.service;

import com.isyaptir.restoran.security.StoragePropertiesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileSystemStorageService {

    private Path rootLocation;
    private StoragePropertiesConfig properties;

    @Autowired
    public FileSystemStorageService(StoragePropertiesConfig properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.properties = properties;
        createDirect(properties.getLocation());

    }

    public String store(MultipartFile file, String path) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        createDirect(properties.getLocation() + path);
        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new RuntimeException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to store file " + filename, e);
        }

        return filename;
    }

    private void createDirect(String path) {
        this.rootLocation = Paths.get(path);
        try {
            Files.createDirectories(rootLocation);
            System.out.println("Dosya");
        } catch (Exception ex) {
            throw new RuntimeException("Hata");
        }
    }
}
