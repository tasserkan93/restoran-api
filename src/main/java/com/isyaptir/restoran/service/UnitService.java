package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Unit;
import com.isyaptir.restoran.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UnitService implements IService<Unit> {

    @Autowired
    private UnitRepository repository;

    @Override
    public Unit findById(String id) {
        Optional<Unit> unit = repository.findById(id);
        unit.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li birim bulunamadı"));
        return unit.get();
    }

    @Override
    public List<Unit> findAll() {
        return (List<Unit>) repository.findAll();
    }

    @Transactional
    @Override
    public Unit delete(Unit unit) {
        try {
            repository.delete(unit);
            return unit;
        } catch (Exception e) {

            return null;
        }
    }

    @Transactional
    @Override
    public Unit save(Unit unit) {
        return repository.save(unit);
    }

    @Transactional
    @Override
    public Unit update(Unit unit) {
        return repository.save(unit);
    }

    public Unit findByCode(String code) {
        return repository.findByCode(code);
    }

}
