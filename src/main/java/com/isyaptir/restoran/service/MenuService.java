package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Menu;
import com.isyaptir.restoran.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class MenuService implements IService<Menu> {


    @Autowired
    private MenuRepository repository;

    @Override
    public Menu findById(String id) {
        Optional<Menu> menu = repository.findById(id);
        menu.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li menu bulunamadı"));
        return menu.get();
    }

    @Override
    public List<Menu> findAll() {
        return (List<Menu>) repository.findAll();
    }

    @Override
    public Menu delete(Menu menu) {

       /* Menu menu = findById(id);
        if (menu!=null){
            menu.setGcRecord(UUID.randomUUID());
            repository.save(findById(id));
            return  menu.getGcRecord();
        }*/
        return null;
    }

    @Override
    public Menu save(Menu menu) {
        return repository.save(menu);
    }

    @Override
    public Menu update(Menu menu) {
        return repository.save(menu);
    }

}
