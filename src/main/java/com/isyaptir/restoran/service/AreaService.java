package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Area;
import com.isyaptir.restoran.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class AreaService implements IService<Area> {

    @Autowired
    private AreaRepository repository;

    @Override
    public Area findById(String id) {
        Optional<Area> area = repository.findById(id);
        area.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li alan Bulunamadı"));
        return area.get();
    }

    @Override
    public List<Area> findAll() {
        return repository.findAll();
    }

    @Transactional
    @Override
    public Area delete(Area area) {
        return repository.save(area);
    }

    @Transactional
    @Override
    public Area save(Area area) {
        return repository.save(area);
    }

    @Transactional
    @Override
    public Area update(Area area) {
        return repository.save(area);
    }

}
