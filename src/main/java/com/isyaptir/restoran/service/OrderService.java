package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Order;
import com.isyaptir.restoran.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements IService<Order> {
    @Autowired
    private OrderRepository repository;

    @Override
    public Order findById(String id) {
        Optional<Order> order = repository.findById(id);
        order.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li sipariş bulunamadı"));
        return order.get();
    }

    @Override
    public List<Order> findAll() {
        return (List<Order>) repository.findAll();
    }

    @Transactional
    @Override
    public Order delete(Order Order) {
        repository.delete(Order);
        return Order;
    }

    @Transactional
    @Override
    public Order save(Order Order) {
        return repository.save(Order);
    }

    @Transactional
    @Override
    public Order update(Order Order) {
        return repository.save(Order);
    }
}
