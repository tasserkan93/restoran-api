package com.isyaptir.restoran.service;

import com.isyaptir.restoran.model.Customer;
import com.isyaptir.restoran.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService implements IService<Customer> {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer findById(String id) {
        Optional<Customer> customer = customerRepository.findById(id);
        customer.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, id + " ID'li müşteri bulunamadı"));
        return customer.get();
    }

    @Override
    public List<Customer> findAll() {
        return (List<Customer>) customerRepository.findAll();
    }

    @Override
    public Customer delete(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer PProduct) {
        return customerRepository.save(PProduct);
    }
}
